<?php
 require_once 'connect.php';
 
session_start();

if(!isset($_SESSION["checkout"]) || $_SESSION["checkout"] !== true){
  header("location: login.php");
  
  exit;
}
?><!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
   
    <link rel="stylesheet" href="newgoal.css">
    <title>Grade Tracker</title>
  </head>
  <body>
    <div id="container">
    <h1 class="center">Hi, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>. Thank you for choosing a project, pleas upload it here:</h1>
    <form action="upload.php" method="post" enctype="multipart/form-data">
 <p> Select a .pdf to upload:</p>
  <input type="file" name="fileToUpload" id="fileToUpload">
  <input type="submit" value="Upload File" name="submit">
</form>

</body>
</html>
