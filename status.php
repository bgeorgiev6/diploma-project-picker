<?php
 require_once 'connect.php';
 session_start();
if(!isset($_SESSION["checkout"]) || $_SESSION["checkout"] !== true){
    header("location: login.php");
    
    exit;
  }
 $status = $grade = $final = "";


$sql = "SELECT * FROM students";
$result = $link->query($sql);

while($row = mysqli_fetch_array($result)){
       if ($_SESSION["id"] == $row["id"] ){
         $grade = $row["grade"];
        $status = $row["projid"];
        if($grade == 0){
          if ($status == 0 ){
            $final = "You have not yet chosen a project";
           }else {
            $final = "Your project is waiting to be graded";
        }
        }else {
          $final = "Your grade is: " . $grade;
      }
       
       }
      
}
if(!isset($_SESSION["checkout"]) || $_SESSION["checkout"] !== true){
  header("location: index.php");
  
  exit;
}

?><!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
   
    <link rel="stylesheet" href="newgoal.css">
    <title>Goal Tracker</title>
  </head>
  <body>
    <div id="container">
    <h1 class="center">Hi, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>. Welcome to your grade checker.</h1>
    <p>The current status of ypur project is:  <b><?php  echo htmlspecialchars($final); ?></b> </p>
    <form action="upload.php" method="post" enctype="multipart/form-data">
    <a href="logout.php" class="btn btn-danger ml-3">Sign Out of Your Account</a>
</form>

</body>
</html>
